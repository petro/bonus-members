
package oblig2;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Random;

public class MemberArchive {

    /**
     * An archive for managing members
     */
    public MemberArchive() {
        this.members = new HashMap<>();

    }

    HashMap<Integer, BonusMember> members;

    /**
     * Method for creating a random member no
     * Takes a random number between 0 and 10000 and checks if it isn't already taken
     * @return the randomly generated member no
     */
    private int findAvailableNumber(){
        Random random = new Random();
        return random.ints(0, 10000)
                .filter(number -> members.get(number)==null)
                .findFirst()
                .getAsInt();
    }

    /**
     * Method for registering points to a member
     * @param memberNo The Member no of the member that gets the points assigned
     * @param points The points that will be assigned to the member
     * @return false if the member doesnt exist, true if it does
     */
    public boolean registerPoints(int memberNo, int points)
    {
        BonusMember member = members.get(memberNo);
        if(member == null){
            return false;
        } else {
            member.registerPoints(points);
            return true;
        }
    }

    /**
     * Method for adding a new member to the archive
     * @param pers The members personal information
     * @param dateEnrolled The date the member enrolled
     * @return memberNo
     */
    public int addMember(Personals pers, LocalDate dateEnrolled) {
        int memberNo = findAvailableNumber();
        members.put(memberNo, new BasicMember(memberNo, pers, dateEnrolled));
        return memberNo;
    }

    /**
     * Method for finding the points of a specific member
     * @param memberNo The Member No of the member
     * @param password The password of the member
     * @return member.getPoints() if the member exits, -1 if not
     */
    public int findPoints (int memberNo, String password){

        BonusMember member = members.get(memberNo);
                if (memberNo == member.getMemberNo() && password.equals(member.okPassword(password))) {
                    return member.getPoints();
                }

                return -1;
        }

    /**
     * Method for checking if members should be upgraded.
     * Checks for upgrade to both silver and gold membership
     * @param date the date used for comparing to th enrolled dates of the members
     */
    public void checkMembers(LocalDate date) {
        members.values().forEach(member -> {
            if (!(member instanceof GoldMember) && (member.findQualificationPoints(date) != 0)) {
                if ((25000 <= member.findQualificationPoints(date))
                        && (member.findQualificationPoints(date) < 75000)
                        && !(member instanceof SilverMember))
                {
                    members.put(member.getMemberNo(),
                            new SilverMember(member.getMemberNo(), member.getPersonals(),
                                    member.getEnrolledDate(), member.getPoints()));
                }
                else if (member.findQualificationPoints(date) <= 75000)
                {
                    members.put(member.getMemberNo(),
                            new GoldMember(member.getMemberNo(), member.getPersonals(),
                                    member.getEnrolledDate(), member.getPoints()));
                }
            }
        });

    }

    /**
     * Getter for bonus members
     * @param memberNo The Member No of the member we need
     * @return member.get(memberNo)
     */
    public BonusMember getMember(int memberNo)
    {
        return members.get(memberNo);
    }
}