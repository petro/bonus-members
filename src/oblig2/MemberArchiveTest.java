package oblig2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * A test-class for the MemberArchive-class
 * Used for testing the methods in the MemberArchive-class
 */
public class MemberArchiveTest {

    private LocalDate testDate;
    private Personals Peter;

    /**
     * Creates a test-date and test-member to use for testing the methods
     */
    @BeforeEach
    void setUp()
    {
        this.testDate = LocalDate.of(2008, 2, 5);
        this.Peter = new Personals("Peter", "Petersen", "peter.p@something.com", "peteR");
    }

    /**
     * Tests if a member gets upgraded to a SilverMember when they have enough points
     * Test-member is given 25000 points , and an test date less than 365 days from the enrolled date
     * which should upgrade them to a SilverMember
     */
    @Test
    void testSilverUpgrade()
    {
        MemberArchive memberArchive = new MemberArchive();
        int b1 = memberArchive.addMember(Peter, testDate);
        memberArchive.registerPoints(b1, 25000);

        memberArchive.checkMembers(LocalDate.of(2009, 2, 2));

        System.out.println("Test 1 : Member upgrades to silver when they have enough points");
        //If the member gets upgraded to a SilverMember the test is completed successfully
        assertTrue(memberArchive.getMember(b1) instanceof SilverMember);
    }

    /**
     * Tests if a member gets upgraded to a GoldMember when they have enough points
     * Test-member is given 75000 points , and an test date less than 365 days from the enrolled date
     * which should upgrade them to a GoldMember
     */
    @Test
    void testGoldUpgrade()
    {
        MemberArchive memberArchive = new MemberArchive();
        int b1 = memberArchive.addMember(Peter, testDate);
        memberArchive.registerPoints(b1, 75000);

        memberArchive.checkMembers(LocalDate.of(2009, 2, 2));

        System.out.println("Test 2 : Member upgrades to gold when they have enough points");
        //If the member gets upgraded to a GoldMember the test is completed successfully
        assertTrue(memberArchive.getMember(b1) instanceof GoldMember);
    }

    /**
     * Checks that members dont get upgraded when missing enough points,
     * or have a too old account
     */
    @Test
    void testNotUpGradedMember()
    {
        MemberArchive memberArchive = new MemberArchive();

        int b1 = memberArchive.addMember(Peter, testDate);
        memberArchive.registerPoints(b1, 15000);

        int b2 = memberArchive.addMember(Peter, testDate);
        memberArchive.registerPoints(b1, 30000);

        memberArchive.checkMembers(LocalDate.of(2010, 2, 3));

        System.out.println("Test 3 : Member not upgraded when missing enough points");
        assertTrue(memberArchive.getMember(b1) instanceof BasicMember);

        System.out.println("Test 4 : Member not upgraded when having enough points, but too old account");
        assertTrue(memberArchive.getMember(b2) instanceof BasicMember);
    }




}
