package oblig2;

import java.time.LocalDate;

/**
 * The BasicMember class is a subclass of the BonusMember class, the main
 * function of the BasicMember class is to represent a standard member.
 */
public class BasicMember extends BonusMember {


    /**
     * The constructor of the BasicMember class
     * @param memberNo The number representing the member
     * @param personals The personal information of the member
     * @param enrolledDate The date the member enrolled the membership
     */
    public BasicMember(int memberNo, Personals personals, LocalDate enrolledDate) {
        super(memberNo, personals, enrolledDate);
    }

}
