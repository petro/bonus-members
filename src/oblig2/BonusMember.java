package oblig2;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * A super-class for BasicMember, SilverMember and GoldMember.
 * Contains information about the member registration date, amount of bonuspoints,
 * personal information and memberID.
 */
public class BonusMember {

    private final int memberNo;
    private final Personals personals;
    private final LocalDate enrolledDate;
    private int bonuspoints = 0;

    /**
     * The constructor for the BonusMember class.
     * @param memberNo the id of the member in int.
     * @param personals the personal information about the member.
     * @param enrolledDate the date the member registered.
     */
    public BonusMember(int memberNo, Personals personals, LocalDate enrolledDate)
    {
        this.memberNo = memberNo;
        this.personals = personals;
        this.enrolledDate = enrolledDate;
        int bonuspoints = 0;
    }

    public BonusMember(int memberNo, Personals personals)
    {
        this.memberNo = memberNo;
        this.personals = personals;
        enrolledDate = getEnrolledDate();
        int bonuspoints = 0;
    }

    public float FACTOR_SILVER = 1.2F;
    public float FACTOR_GOLD = 1.5F;

    /**
     *
     * @return
     */
    public int getMemberNo() {
        return memberNo;
    }

    /**
     * Getter for the personal information
     * @return personals
     */
    public Personals getPersonals() {
        return personals;
    }

    /**
     * Getter for the date that is used to check if the member qualifies for a upgrade.
     * @return enrolledDate
     */
    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    /**
     * Getter for the points that is used to check if the member qualifies for a upgrade.
     * @return bonuspoints
     */
    public int getPoints() {
        return bonuspoints;
    }

    public void setPoints(int points) {
        this.bonuspoints = points;
    }

    /**
     * Checks if a year has gone by since the member registered.
     * If so, then the member doesnt qualify for getting upgraded to silver or gold membership.
     * If a year has not gone by, the member qualifies.
     * @param date
     * @return 0 if the days between registration date and parameter date is over 365 days.
     * @return getBonusPoints if the days between registration date and parameter date is under 365 days.
     */
    public int findQualificationPoints(LocalDate date)
    {
        long daysBetween = ChronoUnit.DAYS.between( this.enrolledDate, date );
        if( daysBetween < 365)
        {
            return getPoints();
        } else
            {
                return 0;
            }
    }

    /**
     * Checks if the password given by the parameter matches
     * the password thats registered with the user.
     * @param password
     * @return true if it matches, false if it doesnt.
     */
    public boolean okPassword(String password)
    {
        return personals.okPassword(password);
    }

    /**
     * Adds an amount of points to the total bonuspoints of the member.
     * @param points the amount of points that should be registered with the user.
     */
    public void registerPoints(int points)
    {
        bonuspoints += points;
    }
}
