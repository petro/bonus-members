package oblig2;

import java.time.LocalDate;

/**
 * The SilverMember class is a subclass of the BonusMember class, the main
 * function of the SilverMember class is to calculate the bonusPoints of a SilverMember.
 */
public class SilverMember extends BonusMember {


    /**
     * Constructor for the SilverMember class
     * @param memberNo The number representing the member
     * @param personals The personal information of the member
     * @param enrolledDate The date the member enrolled in the membership
     * @param bonusPoints the amount of points the member has, used to calculate when upgraded
     */
    public SilverMember(int memberNo, Personals personals, LocalDate enrolledDate, int bonusPoints) {
        super(memberNo, personals, enrolledDate);
        this.setPoints(bonusPoints);

    }

    /**
     * Calculates the members points if they are a SilverMember
     * @param points the amount of points that should be registered with the user.
     */
    @Override
    public void registerPoints(int points)
    {
        super.registerPoints((int) (points * FACTOR_SILVER));
    }
}
